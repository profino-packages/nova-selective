# Laravel Nova Selective

> This repository is based on `slovenianGooner` version of [nova-searchable-select](https://github.com/slovenianGooner/nova-searchable-select).

Description about `Selective` and how it differs from `nova-searchable-select` and `.

## Installation

_Composer_

```
composer require wize-wiz/nova-selective
```

## Usage

Just like the regular select field, but instead of the `options` method you provide the `resource` method
with your resource name.

```php
use WizeWiz\Selective\Selective;
...

SearchableSelect::make('Content', 'content_id')->resource("contents")
... or
SearchableSelect::make("Content", "content_id")->resource(\App\Nova\Content::class)
```

![](usage.gif)

You can pass all the regular options like:

```php
SearchableSelect::make('Content', 'content_id')
    ->resource("contents")
    ->help("Help text")
    ->displayUsingLabels()
    ->nullable()
```

But also three additional options:

```php
SearchableSelect::make('Content', 'content_id')
    ->resource("contents")
    ->label("custom_label_field") // Defaults to the static $title attribute of the resource class
    ->labelPrefix("custom_prefix_field") // Allows you to prefix the label field with one other field, i.e. "code":"label"
    ->value("custom_value_field") // Defaults to 'id'
```

You can now also choose the multiple option. Needs a `text` or `json` field in the database.

```php
SearchableSelect::make('Content', 'content_id')
    ->resource("contents")
    ->multiple()
    ->displayUsingLabels()
    ->nullable()
```

![](multiple.gif)

Another option is to define the maximum number of items shown in the search. (Default: 20)

```php
SearchableSelect::make("Content", "content_id")
                ->resource("contents")
                ->max(10)
```

You can use the base model's search method instead of the Nova resource's search method with `useBaseSearch()`.

```php
SearchableSelect::make('Content', 'content_id')
    ->resource("contents")
    ->useBaseSearch()
```
