import _ from 'lodash';

export const HttpRequests = {

    methods: {
        loadResources(request, callback) {
            callback = callback || function () {};
            request = _.merge({
                endoint: null,
                params: {}
            }, request);

            console.log(request);

            if (this.disabled) {
                this.disabled = false;
            }

            return Nova.request()
                .get(
                    request.endpoint, {
                        params: request.params
                    }
                )
                .then(callback)
                .finally(() => {
                    setTimeout(function () {
                        if (this.initialized === false) {
                            this.inizialized = true;
                        }
                    }.bind(this), 250);
                });
        },
    }

};