Nova.booting((Vue, router, store) => {
    Vue.component('input-selective', require('./components/SearchInput'))
    Vue.component('index-selective', require('./components/IndexField'))
    Vue.component('detail-selective', require('./components/DetailField'))
    Vue.component('form-selective', require('./components/FormField'))
})