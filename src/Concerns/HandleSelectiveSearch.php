<?php

namespace WizeWiz\Selective\Concerns;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

trait HandleSelectiveSearch {

    /**
     * Collect params.
     *
     * @return array
     */
    protected function selectiveCollectParams() {
        $Request = request();

        $params = [];

        $params['search'] = $Request->get('search', null);
        $params['value'] = $Request->get('value', 'id');

        // api options
        $options = $Request->get('apiOptions', []);
        if(is_string($options)) {
            $options = json_decode($options, true);
        }
        $params['options'] = array_merge(
            isset($this->selective_default_options) ? $this->selective_default_options : []
        , $options);

        // dependency values
        $dependencies = $Request->get('dependencyValues', []);
        if(is_string($dependencies)) {
            $dependencies = json_decode($dependencies, true);
        }
        $params['dependencies'] = $dependencies;

        return $params;
    }

    /**
     * @param Request $Request
     * @param $resources
     * @return mixed
     */
    protected function selectiveResponse($resources) {
        $Request = request();
        $params = $this->selectiveCollectParams();

        $response = [
            'label' => 'The Label',
            'resources' => collect($resources)->map(function($resource) use ($params) {

                $return = [];
                $options = $params['options'];

                if(is_array($resource)) {
                    $resource = (object) $resource;
                }
                if(is_object($resource)) {

                    // @todo: check if set
                    $return['id'] = $resource->{$params['value']};
                    // @todo: check if set
                    if(!empty($options['displayFormat'])) {
                        $placeholders = [];
                        $return['display'] = $options['displayFormat'];
                        if(preg_match_all("~%([^%]+)%~", $options['displayFormat'], $placeholders) !== false) {
                            foreach($placeholders[1] as $index => $placeholder) {
                                if(isset($resource->{$placeholder} )) {
                                    $return['display'] = str_replace($placeholders[0][$index], $resource->{$placeholder}, $return['display']);
                                }
                            }
                        }
                    }
                    else {
                        $return['display'] = isset($resource->{$options['display']}) ? $resource->{$options['display']} : '--';
                    }

                    if(isset($resource->avatar)) {
                        $return['avatar'] = $resource->avatar;
                    }

                    return $return;
                }
                else {
                    return [
                        'id' => 'todo --',
                        'display' => 'todo --'
                    ];
                }
            })->toArray()
        ];

        return response()->json($response);
    }

}