<?php

namespace WizeWiz\Selective\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravel\Nova\Http\Requests\ResourceIndexRequest;

class SelectiveController extends Controller
{

    /**
     * Get resource ID's from request.
     *
     * @param Request $request
     * @param $key
     * @return mixed
     */
    protected function getResourceIdsFromRequest(Request $request, $key) : array {
        $ids = $request->get($key);
        if(is_array($ids)) {
            return $ids;
        }
        $ids = json_decode($ids, true);
        return !empty($ids) ? $ids : [];
    }

    /**
     * @param ResourceIndexRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ResourceIndexRequest $request)
    {
        $searchable = $request->get("searchable", false);
        $resource = $request->resource();
        $label = $request->get("label", $resource::$title);
        $labelPrefix = $request->get("labelPrefix", false);
        $value = $request->get("value");

        $ignore_ids = $request->has("ignore_resource_ids");
        $use_ids = $request->has("use_resource_ids");

        if ($searchable && $request->filled('search')) {
            $items = $request->model()::search($request->get('search'));
        } else {
            $items = $request->toQuery();
        }

        if ($use_ids || $ignore_ids) {
            $ids = $this->getResourceIdsFromRequest($request, 'resource_ids');
            if($use_ids) {
                $items->whereIn($value, $ids);
            }
            if($ignore_ids) {
                $items = $items->whereNotIn($value, $ids);
            }
        }

        if ($request->has("max")) {
            $items = $items->take($request->get("max"));
        }
    
        if (!$searchable) {
            // Don't apply the relatableQuery if not searchable, it won't handle it
            // @todo: but this doesn't do anything? query is not being saved anywhere ..
            $request->resource()::relatableQuery($request, $items);
        }

        $items = $items->get()->makeVisible(['display', 'value'])->each(function ($item) use ($request, $labelPrefix, $label, $value) {
            $item->display = '';
            if($labelPrefix) {
                $item->display .= $item->{$labelPrefix} . ': ';
            }
            $item->display .= $item->{$label};
            $item->value = $item->{$value};
        });

        $resource = $request->resource();

        return response()->json([
            "label" => $resource::label(),
            "resources" => $items,
        ]);
    }

    /**
     * Get the paginator instance for the index request.
     *
     * @param  \Laravel\Nova\Http\Requests\ResourceIndexRequest  $request
     * @param  string  $resource
     * @return \Illuminate\Pagination\Paginator
     */
    protected function paginator(ResourceIndexRequest $request, $resource)
    {
        return $request->toQuery()->simplePaginate(
            $request->viaRelationship()
                        ? $resource::$perPageViaRelationship
                        : ($request->perPage ?? 25)
        );
    }
}
