<?php

namespace WizeWiz\Selective;

use Illuminate\Support\Facades\Log;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Nova;

class Selective extends Select
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'selective';

    public function __construct($name, $attribute = null, callable $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);
        $this->withMeta([
            "label" => null,
            "labelPrefix" => null,
            "valueField" => "id",
            "property" => "id",
            "isMultiple" => false,
            "max" => 20,
            "searchable" => false,
            "searchableResource" => null,
            "api" => null,
            "apiOptions" => null,
            "dependsOn" => null,
            "dependsOnSettings" => [
                "disableWhenEmpty" => false
            ],
            "closeOnSelect" => true,
            "settings" => [
                'closeOnSelect' => true,
                'clearOnClose' => true,
                'showClearAll' => true
            ]
        ]);
    }

    public function max($max)
    {
        return $this->withMeta([
            "max" => $max
        ]);
    }

    public function multiple()
    {
        return $this->withMeta([
            "isMultiple" => true
        ]);
    }

    public function resource($name)
    {
        // Find searchable resource based on name if it's a class
        if (class_exists($name)) {
            $name = $name::uriKey();
        }
        $meta = [
            "searchableResource" => $name
        ];

        if (!isset($this->meta["label"])) {
            $resource = Nova::resourceForKey($name);
            $meta["label"] = $resource::$title;
        }

        return $this->withMeta($meta);
    }

    public function api($api, array $options = []) {
        $options = array_merge([
            'query' => [],
            'display' => 'name',
            'searchable' => ['name']
        ], $options);

        if(!is_array($options['searchable'])) {
            $options['searchable'] = (array) $options['searchable'];
        }

        return $this->withMeta([
            'api' => $api,
            'apiOptions' => $options
        ]);
    }

    public function dependsOn($name, array $settings = []) {
        return $this->withMeta([
            'dependsOn' => $name,
            'dependsOnSettings' => array_merge($this->meta['dependsOnSettings'], $settings)
        ]);
    }

    public function label($label)
    {
        return $this->withMeta([
            "label" => $label
        ]);
    }

    public function labelPrefix($labelPrefix)
    {
        return $this->withMeta([
            "labelPrefix" => $labelPrefix
        ]);
    }

    /**
     * @deprecated
     */
    public function value($value)
    {
        return $this->property($value);
    }

    public function property($value)
    {
        return $this->withMeta([
            "valueField" => $value, // compatability
            "property" => $value // new
        ]);
    }

    public function displayUsingLabels()
    {
        // @todo: refactor to support API results.
        return $this->displayUsing(function ($value) {
            // @todo: check for resource !== null?
            $model = Nova::modelInstanceForKey($this->meta["searchableResource"]);
            if (!$model) {
                return $value;
            }

            $labelValue = $model->where($this->meta["valueField"], $value)->value($this->meta["label"]);

            if (!$labelValue) {
                return $value;
            }

            return $labelValue;
        });
    }

    public function loadResourcesOnNew()
    {
        return $this->withMeta([
            "loadResourcesOnNew" => true
        ]);
    }

    public function useBaseSearch($useBaseSearch = true)
    {
        return $this->withMeta([
            "searchable" => $useBaseSearch
        ]);
    }

    public function settings(array $settings)
    {
        return $this->withMeta([
            "settings" => $settings
        ]);
    }

}
