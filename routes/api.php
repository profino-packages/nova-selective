<?php

use WizeWiz\Selective\Http\Controllers\SelectiveController;

Route::get('/{resource}', SelectiveController::class."@index");
